import time

from pages.home_page import HomePage

from tests.base_test import BaseTest


class HomePageTest(BaseTest):

    def test_title_of_page(self):
        home_page = HomePage(self.driver)
        home_page.go_to_site()
        self.assertTrue("1StopBedrooms" in self.driver.title)

    def test_search_by_brand(self):
        search_param = "A.R.T."
        home_page = HomePage(self.driver)
        home_page.go_to_site()
        home_page.type_search_parameter(search_param)
        home_page.click_search_button()
        assert search_param in home_page.driver.title
