from pages.search_result_page import SearchResultPage
from tests.base_test import BaseTest


class SearchResultPageTests(BaseTest):
    def test_sort_by_select(self):
        search_result_page = SearchResultPage(self.driver)
        search_result_page.open_page("/catalogsearch/result?q=acme")
        search_result_page.select_sort_by_option()
        self.assertTrue("dir=desc" in self.driver.current_url)
