import unittest
from selenium.webdriver.chrome.options import Options

from selenium import webdriver
from selenium.webdriver.chrome import options


class BaseTest(unittest.TestCase):

    def setUp(self):
        options = Options()
        options.add_argument("--start-fullscreen")

        self.driver = webdriver.Chrome(options=options, executable_path="../chromedriver.exe")

    def tearDown(self):
        self.driver.close()

    # def browser():
    #     driver = webdriver.Chrome(executable_path="../chromedriver.exe")
    # yield driver
    # driver.quit()
