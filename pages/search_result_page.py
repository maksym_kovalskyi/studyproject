from studyproject.pages.base_page import BasePage
from studyproject.utils.locators import SearchResultPageLocators


class SearchResultPage(BasePage):
    def __init__(self, driver):
        self.locator = SearchResultPageLocators
        super().__init__(driver)

    def select_sort_by_option(self):
        self.find_element(self.locator.LOCATOR_SORT_BY_SELECT).click()
        self.find_element(self.locator.LOCATOR_SORT_BY_SELECT_HIGH_LOW_OPTION).click()
        return SearchResultPage(self.driver)

