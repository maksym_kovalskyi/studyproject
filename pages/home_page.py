import time

from pages.base_page import BasePage
from utils.locators import HomePageLocators


class HomePage(BasePage):
    def __init__(self, driver):
        self.locator = HomePageLocators
        super().__init__(driver)

    def type_search_parameter(self, search_parameter):
        search_field = self.find_element(self.locator.LOCATOR_SEARCH_FIELD)
        search_field.click()
        search_field.clear()
        search_field.send_keys(search_parameter)
        return HomePage(self.driver)

    def click_search_button(self):
        search_button = self.find_element(self.locator.LOCATOR_SEARCH_BUTTON)
        search_button.click()
        time.sleep(0)
        return HomePage(self.driver)

    def return_browser(self):
        return self.driver
