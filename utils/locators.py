from selenium.webdriver.common.by import By


class HomePageLocators:
    LOCATOR_SEARCH_FIELD = (By.CSS_SELECTOR, "input#search")
    LOCATOR_SEARCH_BUTTON = (By.CSS_SELECTOR, "button[title = 'Search']")


class SearchResultPageLocators:
    LOCATOR_SORT_BY_SELECT = (By.XPATH, ".//div[@class = 'sort-by-mobile']/div/span")
    LOCATOR_SORT_BY_SELECT_RELEVANCE_OPTION = (By.CSS_SELECTOR, "ul.sort-by-select :nth-child(1)")
    LOCATOR_SORT_BY_SELECT_LOW_HIGH_OPTION = (By.CSS_SELECTOR, "ul.sort-by-select :nth-child(2)")
    LOCATOR_SORT_BY_SELECT_HIGH_LOW_OPTION = (By.CSS_SELECTOR, "ul.sort-by-select :nth-child(3)")